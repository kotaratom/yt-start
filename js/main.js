//Author: Tomas Kotara

// Set site warning
document.cookie = 'cross-site-cookie=bar; SameSite=None; Secure';

function authenticate() {
    return gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/youtube.force-ssl"})
        .then(function () {
                console.log("Sign-in successful");
            },
            function (err) {
                console.error("Error signing in", err);
            });
}

function loadClient() {
    gapi.client.setApiKey("AIzaSyCnioyRuTtNnE6laE3S6u26nQH3erkhKnQ");
    return gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
        .then(function () {
                console.log("GAPI client loaded for API");
            },
            function (err) {
                console.error("Error loading GAPI client for API", err);
            });
}

function execute() {
    let question = document.getElementById("myInput").value;
    console.log(question);
    return gapi.client.youtube.search.list({
        "part": "snippet",
        "q": question,
        "maxResults": 10
    })
        .then(function (response) {
                // Handle the results here (response.result has the parsed body).
                console.log("Response", response);

                if (response.result.pageInfo.totalResults > 0) {
                    setStorage(response);
                    printVideos(response);
                } else {
                    searchError();
                }
            },
            function (err) {
                console.error("Execute error", err);
                searchError();
            });
}

gapi.load("client:auth2", function () {
    gapi.auth2.init({client_id: "1035781625176-nd2pdejk7lrltvrrjda9khtcln87p37d.apps.googleusercontent.com"});
});


// Printing videos
function printVideos(response) {
    // Returned response, set to items
    let resultsItm = response.result.items;
    let sortedResultsItm = resultsItm.sort(customSort);
    printItems(sortedResultsItm);
}

// Printing videos reverse
function printVideosReverse(response) {
    // Returned response, set to items
    let resultsItm = response.result.items;
    let sortedResultsItm = resultsItm.sort(customSort).reverse();
    printItems(sortedResultsItm);
}

// Print videos
function printItems(sortedResultsItm) {
    let block = document.getElementById("block");
    block.innerHTML = "";

    for (let i = 0; i < sortedResultsItm.length; i++) {

        // Print item
        block.innerHTML += "<div class=\"embed-responsive embed-responsive-16by9 mt-3\">" +
            "<iframe id=\"" + i + "\" class=\"embed-responsive-item\" src=\".\" allowfullscreen></iframe>" +
            "</div>";

        // Print datetime
        block.innerHTML += "<p class=\"mt-2\">" + new Date(sortedResultsItm[i].snippet.publishTime).toDateString() + "</p>";

        document.getElementById(i).src = "https://www.youtube.com/embed/" + sortedResultsItm[i].id.videoId;

        // Control print
        console.log(i);
        console.log(sortedResultsItm[i].snippet.publishTime);
    }
}

// Sort
function customSort(a, b) {
    return new Date(a.snippet.publishTime).getTime() - new Date(b.snippet.publishTime).getTime();
}

// Asc sort
function ascSort() {
    let data = localStorage.getItem("searchResponse");
    if (data) {
        let response = JSON.parse(data);
        printVideos(response);
    } else {
        dataError();
    }
}

// Dsc sort
function dscSort() {
    let data = localStorage.getItem("searchResponse");
    if (data) {
        let response = JSON.parse(data);
        printVideosReverse(response);
    } else {
        dataError();
    }
}

// Locale storage
function setStorage(response) {
    localStorage.setItem(
        "searchResponse",
        JSON.stringify(response)
    );
}

// Print error
function searchError() {
    let block = document.getElementById("block");
    block.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">No data, please repeat!</div>";
}

// Data error
function dataError() {
    let block = document.getElementById("block");
    block.innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">No data to sort!</div>";
}
